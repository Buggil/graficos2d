

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gil
 */



public class Matriz  {
   
     public void Matrix(){
      
     }
    
    
    
    public double[] Traslacion_2D(double x, double y, int Vtx,int Vty)
    {
        double[]xy=new double [3];
        xy[0]=x;xy[1]=y;xy[2]=1;
        
        double [][]VT= new double[3][3];
        
        for(int i=0;i<3;i++)
            for(int j=0;j<3;j++)
            {
                if(i==j)
                    VT[i][j]=1;
                
                else VT[i][j]=0;
            }
        
        VT[0][2]=Vtx;VT[1][2]=Vty;
        
        double[]trasf=new double[3];
        for(int i=0;i<3;i++)
            for(int j=0;j<3;j++)
            {
                trasf[i]+=VT[i][j]*xy[j];
            }
        
        return trasf;
    }
    
    public double[]Escalado_2D(double x, double y, double Vsx,double Vsy)
    {
        double[]xy=new double[3];
        xy[0]=x;xy[1]=y;xy[2]=1;
        
        double [][]VT= new double[3][3];
        
        for(int i=0;i<3;i++)
            for(int j=0;j<3;j++)
            {
                if(i==j)
                {
                    if(i==0)
                        VT[i][j]=Vsx;
                    
                     if(i==1)
                        VT[i][j]=Vsy;
                     
                     if(i==2)
                        VT[i][j]=1;
                }
                
                else VT[i][j]=0;
            }
       
        
        double[]escdo=new double[3];
        for(int i=0;i<3;i++)
            for(int j=0;j<3;j++)
            {
                escdo[i]+=VT[i][j]*xy[j];
            }

        return escdo;
    }
    
    
     public double[]Rotacion_2D(double x, double y,Double Angulo)
    {
        double []xy=new double[3];
        xy[0]=x;xy[1]=y;xy[2]=1;
        
        double [][]VT= new double[3][3];
        
        for(int i=0;i<3;i++)//Rellenado de la matriz  de Rotacion
            for(int j=0;j<3;j++){
               
                if(i==j){
                    if(i==2)
                        VT[i][j]=1;
                    
                    else  VT[i][j]=Math.cos((Angulo*Math.PI)/(180));                                                          
                }
                
                else if(i==0&&j==1)
                    VT[i][j]=Math.sin((Angulo*Math.PI)/(180))*(-1);
                
                else if(i==1&&j==0)
                    VT[i][j]=Math.sin((Angulo*Math.PI)/(180));
                
               else VT[i][j]=0;
            }
       
        
        double[]rotar=new double[3];
        for(int i=0;i<3;i++)
            for(int j=0;j<3;j++)
            {
                rotar[i]+=VT[i][j]*xy[j];//Calculo del punto de la rotacion
            }
                            
        return rotar;
    }
     public double[] Traslacion_3D(double x, double y,double z, int Vtx,int Vty,int Vtz)
    {
        double[]xyz=new double[4];
        xyz[0]=x ;xyz[1]=y ; xyz[2]=z; xyz[3]=1;// contiene el punto a trasladar
        
        double [][]VT= new double[4][4];//Matriz de traslacion
        
        for(int i=0;i<4;i++)//rellenado de la matriz de traslacion
            for(int j=0;j<4;j++)
            {
                if(i==j)
                    VT[i][j]=1;
                
                else VT[i][j]=0;
            }
        
        VT[0][3]=Vtx;VT[1][3]=Vty;VT[2][3]=Vtz;//Asignacion del vector de traslacion en la Matriz  de trsln
        
        double[]trasf=new double[4];//Contenedor para el nuevo punto trsladado
        for(int i=0;i<4;i++)
            for(int j=0;j<4;j++)
            {
                trasf[i]+=VT[i][j]*xyz[j];//Calculo
            }
                 
          return trasf;
        
    }
     
     public double[]Escalado_3D(double x, double y, double z,double Vsx,double Vsy,double Vsz)
    {
       double[]xyz=new double[4];
        xyz[0]=x ;xyz[1]=y ; xyz[2]=z; xyz[3]=1;// contiene el punto a escalar
        
        double [][]VT= new double[4][4];
        
        for(int i=0;i<4;i++)//Relleno para la matrix de escalado
            for(int j=0;j<4;j++)
            {
                if(i==j)
                {
                    if(i==0)
                        VT[i][j]=Vsx;
                    
                     if(i==1)
                        VT[i][j]=Vsy;
                     
                     if(i==2)
                        VT[i][j]=Vsz;
                     
                     if(i==3)
                        VT[i][j]=1;
                }
                
                else VT[i][j]=0;
            }
       
        
        double[]escdo=new double[4];
        for(int i=0;i<4;i++)
            for(int j=0;j<4;j++)
            {
                escdo[i]+=VT[i][j]*xyz[j];//Calculo del escalado dl punto
            }
    
        return escdo;
    }
     
      public double[]Rotacion_3D_X(double x, double y,double z,Double Angulo)
    {
        double []xyz=new double[4];
        xyz[0]=x;
        xyz[1]=y;
        xyz[2]=z;
        xyz[3]=1;//Vector con punto a rotar
        
        double [][]VT= new double[4][4];
        
        for(int i=0;i<4;i++)//Relleno de la matrix a arotar respectoa X
            for(int j=0;j<4;j++){
               
                if(i==j){
                    if(i==1){

                     VT[i][j]=Math.cos((Angulo*Math.PI)/(180)); 
                     VT[i][j+1]=Math.sin((Angulo*Math.PI)/(180))*(-1);
                    }
                    
                    if(i==2){
                      VT[i][j]=Math.cos((Angulo*Math.PI)/(180));
                      VT[i][j-1]=Math.sin((Angulo*Math.PI)/(180));
                    }
                }
                
               if(VT[i][j]==0)
                   VT[i][j]=0;
            }
                 VT[0][0]=1;VT[3][3]=1;
       
        
        double[]rotar=new double[4];
        for(int i=0;i<4;i++)
            for(int j=0;j<4;j++)
            {
                rotar[i]+=VT[i][j]*xyz[j];//Calculo del Punto rotado rpto a X
            }
            
        return rotar;
    }
public double[]Rotacion_3D_Y(double x, double y,double z,Double Angulo)
    {
        double []xyz=new double[4];
        xyz[0]=x;  
        xyz[1]=y;  
        xyz[2]=z; 
        xyz[3]=1.00;//vECTOR del punto a rotar
        
        double [][]VT= new double[4][4];
        
        for(int i=0;i<4;i++)
            for(int j=0;j<4;j++){//Relleno de la matrix de rotacion rspect a y
               
                if(i==j){
                    if(i==0){

                     VT[i][j]=Math.cos((Angulo*Math.PI)/(180.00)); 
                     VT[i][j+2]=Math.sin((Angulo*Math.PI)/(180.00));
                    }
                    
                    if(i==2){
                      VT[i][j]=Math.cos((Angulo*Math.PI)/(180.00));
                      VT[i][j-2]=Math.sin((Angulo*Math.PI)/(180.00))*(-1.00);
                    }
                    
                    if(VT[i][ j]==0)
                            VT[i][j]=1.00;
                }
                
               if(VT[i][ j]==0.00)
                   VT[i][j]=0.00;
            }
        
        double[]rotar=new double[4];
        for(int i=0;i<4;i++)
            for(int j=0;j<4;j++)
            {
                rotar[i]+=VT[i][j]*xyz[j];//Calculo del punto rotado
            }

        return rotar;
    }

public double[]Rotacion_3D_Z(double x, double y,double z,Double Angulo)
    {
        double []xyz=new double[4];
        xyz[0]=x;
        xyz[1]=y;
        xyz[2]=z;
        xyz[2]=1.00;//Vector con el punto a rotar
        
        double [][]VT= new double[4][4];
        
        for(int i=0;i<4;i++)//Relleno de la matriz de rotacion rpcto a Z
            for(int j=0;j<4;j++){
               
                if(i==j){
                    if(i==0){

                     VT[i][j]=Math.cos((Angulo*Math.PI)/(180.00)); 
                     VT[i][j+1]=Math.sin((Angulo*Math.PI)/(180.00))*(-1.00);
                    }
                    
                    if(i==1){
                      VT[i][j]=Math.cos((Angulo*Math.PI)/(180.00));
                      VT[i][j-1]=Math.sin((Angulo*Math.PI)/(180.00));
                    }
                    
                    if(VT[i][ j]==0)
                            VT[i][j]=1;
                }
                
               if(VT[i][ j]==0)
                   VT[i][j]=0;
            }
        
        double[]rotar=new double[4];
        for(int i=0;i<4;i++)
            for(int j=0;j<4;j++)
            {
                rotar[i]+=VT[i][j]*xyz[j];//Calculo del punto rotado
            }

        return rotar;
    }
 
}


